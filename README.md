# cloud-storage-lite

A simple, flexible Google Cloud Storage client (GCS).

This library isn't as featureful as, say the [cloud-storage](https://crates.io/crates/cloud-storage) crate, but it's also more usable if you:
* are using a secret keeper like [Vault](https://www.vaultproject.io)
* generally work with a single bucket (using a bucket-scoped client)
* would like to use multiple clients or tokio runtimes in one program
* want canonicalized errors (404 gets one and only one error variant)

## Example

```rust
use std::{error::Error, convert::Infallible};
use cloud_storage_lite::{
 self as gcs,
 client::BucketClient,
 token_provider::{
   self,
   oauth::{self, OAuthTokenProvider, ServiceAccount}},
};
use futures::{future, stream, TryStreamExt};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + 'static>> {
  let token_provider =
    token_provider::RenewingTokenProvider::new(
      OAuthTokenProvider::new(
        ServiceAccount::read_from_canonical_env()?,
        oauth::SCOPE_STORAGE_FULL_CONTROL,
      )?
    );
  let client = gcs::Client::new(token_provider)
    .into_bucket_client("my-bucket".into());

  client
    .create_object(
      "key",
      stream::once(
        future::ok::<_, Infallible>(b"value".to_vec())
      )
    )
    .await?;

  let object = client.get_object("key").await?;

  let value_bytes = client
    .download_object(&object.name)
    .await?
    .map_ok(|chunk| chunk.to_vec())
    .try_concat()
    .await?;

  println!(
    "the value is: {}",
    String::from_utf8(value_bytes)?
  );

  Ok(())
}
```
