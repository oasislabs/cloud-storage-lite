//! Errors that occur while using this library.

use thiserror::Error;

pub use crate::api::GoogleError;

/// An Error produced by this library.
#[derive(Debug, Error)]
pub enum Error {
    /// An HTTP error occurred. If a not-unspecified error response is received, it
    /// will be converted into a more specific `Error` variant. For instance, a 404
    /// response will turn into `Error::NotFound`.
    #[error("an http error occurred: {0}")]
    Http(#[from] reqwest::Error),

    /// The requested resource could not be found.
    #[error(transparent)]
    NotFound(#[from] NotFoundError),

    /// The requested resource could not be fetched due to lack of permissions.
    #[error("permission denied: {0}")]
    PermissionDenied(String),

    /// The GCP API returned some error that's not commonly encountered while using this library.
    #[error(transparent)]
    OtherGoogle(#[from] GoogleError),

    /// The [`TokenProvider`] had an error.
    #[error("couldn't fetch token: {0}")]
    TokenFetch(anyhow::Error),
}

/// The specific thing that wasn't found.
#[derive(Debug, Error)]
pub enum NotFoundError {
    /// The bucket wasn't found.
    #[error("the bucket `{bucket}` was not found")]
    Bucket {
        /// The name of the bucket that wasn't found.
        bucket: String,
    },

    /// An object wasn't found, but the bucket does exist.
    #[error("the object `{bucket}/{key}` was not found")]
    Object {
        /// The name of the bucket from which the object was requested.
        bucket: String,
        /// The object key that wasn't found.
        key: String,
    },

    /// Some other thing wasn't found.
    #[error("{0}")]
    Other(String),
}
